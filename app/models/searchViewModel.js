"use strict";

const BaseViewModel = require('./BaseViewModel');

/**
 * View model representing the Search Page
 */
class SearchViewModel extends BaseViewModel {
    constructor() {
        super();
        this.title = "Search";
        this.searchTerm = "";
        this.searchResults = [];
    }
}

module.exports = SearchViewModel;
