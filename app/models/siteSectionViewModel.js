"use strict";

const BaseViewModel = require('./BaseViewModel');

/**
 * View model representing the Site Sections for listing
 * on the home page
 */
class SiteSectionViewModel extends BaseViewModel {
    constructor() {
        super();
        this.description = "";
        this.tag = "";
        this.documentCount = 0;
    }
}

module.exports = SiteSectionViewModel;
