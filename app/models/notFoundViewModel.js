"use strict";

const BaseViewModel = require('./BaseViewModel');

/**
 * View model representing the Home Page
 */
class NotFoundViewModel extends BaseViewModel {
    constructor() {
        super();
        this.slug = "";
    }
}

module.exports = NotFoundViewModel;
