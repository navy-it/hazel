"use strict";

const BaseViewModel = require('./BaseViewModel');

/**
 * View model representing the Edit Page
 */
class EditViewModel extends BaseViewModel {
    constructor() {
        super();
        this.document = null;
    }
}

module.exports = EditViewModel;
