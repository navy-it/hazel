"use strict";

/**
 * View model representing the Detail Page
 */
class BaseViewModel {
    constructor() {
        this.title = "";
        this.config = {};
        this.session = {};
        this.error = false;
        this.success = false;
    }
}

module.exports = BaseViewModel;
