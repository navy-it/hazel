"use strict";

const BaseViewModel = require('./BaseViewModel');

/**
 * Represents a single document in the
 * Hazel system
 */
class Document extends BaseViewModel {
    constructor() {
        super();
        this.slug = "";
        this.markdown = "";
        this.html = "";
        this.updateDate = null;
        this.createDate = null;
        this.tags = [];
    }
}

module.exports = Document;
