"use strict";

const BaseViewModel = require('./BaseViewModel');

/**
 * View model representing the Home Page
 */
class HomeViewModel extends BaseViewModel {
    constructor() {
        super();
        this.title = "Home";
        this.popularSearches = [];
        this.recentDocuments = [];
        this.randomDocuments = [];
        this.popularDocuments = [];
        this.siteSections = [];
    }
}

module.exports = HomeViewModel;
