"use strict";

const BaseViewModel = require('./BaseViewModel');

/**
 * View model representing the Detail Page
 */
class DetailViewModel extends BaseViewModel {
    constructor() {
        super();
        this.document = null;
        this.relatedDocuments = [];
        this.recentDocuments = [];
    }
}

module.exports = DetailViewModel;
