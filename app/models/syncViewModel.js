"use strict";

const BaseViewModel = require('./BaseViewModel');

/**
 * View model representing the Sync Page
 */
class SyncViewModel extends BaseViewModel {
    constructor() {
        super();
        this.syncKey = "";
    }
}

module.exports = SyncViewModel;
