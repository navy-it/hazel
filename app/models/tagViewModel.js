"use strict";

const BaseViewModel = require('./BaseViewModel');

/**
 * View model representing the Tag Page
 */
class TagViewModel extends BaseViewModel {
    constructor() {
        super();
        this.title = "Tag";
        this.popularSearches = [];
        this.taggedDocuments = [];
    }
}

module.exports = TagViewModel;
