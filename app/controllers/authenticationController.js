"use strict";

const HomeViewModel = require("../models/homeViewModel");
const bcrypt = require('bcrypt');
const uuidv4 = require('uuid/v4');

class AuthenticationController {
    constructor(server, config, authMethod, storageProvider, emailProvider) {
        this._server = server;
        this._config = config;
        this._auth = authMethod;
        this._storageProvider = storageProvider;
        this._emailProvider = emailProvider;
        console.log(this._emailProvider.send);

        this._bindRoutes();
    }

    _bindRoutes() {
        // /search
        //if (this._auth !== "all" || this._auth !== "admin")
        //    return;
        this._server.get("/login", this.authMethod.bind(this), this.loginPage.bind(this));
        this._server.post("/login", this.logIn.bind(this));
        this._server.get("/logout", this.logOut.bind(this));
        this._server.get("/register", this.authMethod.bind(this), this.registerPage.bind(this));
        this._server.post("/register", this.register.bind(this));
        this._server.get("/register/validate", this.authMethod.bind(this), this.validatePage.bind(this));
        this._server.get("/register/validate/:email/:code", this.authMethod.bind(this), this.doValidate.bind(this));
        this._server.get("/register/validate/resend", this.authMethod.bind(this), this.resendValidationEmail.bind(this));
        this._server.post("/register/validate/resend", this.resendValidationEmailPost.bind(this));
    }

    authMethod(req, res, next) {
        if (req.session.userId) {
            return res.redirect('/');
        }

        return next();
    }

    register(req, res, next) {
        if (req.body.password[0] !== req.body.password[1]) {
            return res.send("Passwords must match");
        }

        var valid = this._config.validateUser(req.body);
        if (!valid.isValid) {
            var viewModel = new HomeViewModel();
            viewModel.session = req.session;
            viewModel.config = this._config;
            viewModel.error = valid.error;

            return res.render("signup", viewModel);
        }

        bcrypt.hash(req.body.password[0], 10, function(err, hash) {
            if (err) {
                console.error(err);
                return res.send('There was an error processing your request');
            }

            this._storageProvider.readObject('User', {email: req.body.email}, (err, obj) => {
                if (err) {
                    console.error(err);
                    return res.send("There was an error processing your request.");
                }

                if (obj.length > 0) {
                    console.log('Found a user by that email');
                    //The email address was already registered. Send the user
                    //a password reset email. Don't let the requester know
                    //the account already exists.
                    this._emailProvider.send({
                        template: 'alreadyRegistered',
                        message: {
                            to: req.body.email,
                            subject: 'NavyIT Password Reset'
                        }
                    })
                    .then(() => {
                        res.redirect('/register/validate');
                    })
                    .catch(console.error);
                } else {
                    //The email wasn't registered, add it to the db
                    this._storageProvider.storeObject('User', {
                        email: req.body.email,
                        password: hash
                    }, (err, user) => {
                        if (err) {
                            console.error(err);
                            return res.send('There was an error processing your request');
                        }

                        this.sendValidationEmail(req.body.email, (err) => {
                            if (err) {
                                console.error(err);
                                return res.send('There was an error processing your request');
                            }

                            return res.redirect('/register/validate');
                        });
                    });
                }
            });
        }.bind(this));
    }

    resendValidationEmail(req, res, next) {
        var viewModel = new HomeViewModel();
        viewModel.session = req.session;

        viewModel.config = this._config;

        return res.render("resendValidationEmail", viewModel);
    }

    resendValidationEmailPost(req, res, next) {
        if (!req.body.email) {
            return res.redirect("/register/validate/resend");
        }

        this.sendValidationEmail(req.body.email, (err) => {
            if (err) {
                console.error(err);
                return res.send('There was an error processing your request.');
            }

            res.redirect("/register/validate");
        });
    }

    sendValidationEmail(email, callback) {
        var code = uuidv4();
        this._storageProvider.updateObject('UserRegistration', {email: email}, {
            email: email,
            code: code
        }, (err) => {
            if (err) {
                console.error(err);
                return callback('There was an error processing your request');
            }

            this._emailProvider.send({
                template: 'register',
                message: {
                    to: email,
                    subject: "Welcome to NavyIT"
                },
                locals: {
                    code: code,
                    email: email
                }
            });

            callback();
        });
    }

    registerPage(req, res, next) {
        var viewModel = new HomeViewModel();
        viewModel.session = req.session;

        viewModel.config = this._config;

        res.render("signup", viewModel);
    }

    validatePage(req, res, next) {
        var viewModel = new HomeViewModel();
        viewModel.session = req.session;
        viewModel.config = this._config;

        res.render("validate", viewModel);
    }

    doValidate(req, res, next) {
        let email = req.params.email;
        let code = req.params.code;

        this._storageProvider.readObject('UserRegistration', {email: email, code: code}, (err, objs) => {
            if (err) {
                console.error(err);
                return res.send('There was an error validating your account. Please try again.');
            }

            if (objs.length === 0) {
                //That was an invalid code + email pair.
                console.log("An invalid code / email pair was used to validate an email address.");
                var viewModel = new HomeViewModel();
                viewModel.session = req.session;
                viewModel.config = this._config;
                viewModel.error = "An invalid code and email pair was provided. Please try again";

                res.render("validate", viewModel);
            }

            this._storageProvider.updateObject('User', {email: email}, { validated: true }, (err, saved) => {
                if (err) {
                    console.error(err);
                    return res.send('There was an error validating your account. Please try again');
                }

                this._storageProvider.removeObject('UserRegistration', {email: email, code: code}, (err) => {
                    if (err) { 
                        console.error(err); 
                    }

                    //Render the login screen with success message
                    var viewModel = new HomeViewModel();
                    viewModel.session = req.session;
                    viewModel.config = this._config;
                    viewModel.success = "Email verified successfully"

                    res.render("login", viewModel);
                });
            });
        });
    }

    loginPage(req, res, next) {
        var viewModel = new HomeViewModel();
        viewModel.session = req.session;
        viewModel.config = this._config;

        res.render("login", viewModel);
    }

    /**
     * Handle login
     */
    logIn(req, res, next) {
        this._storageProvider.readObject('User', {email: req.body.email}, function(err, users) {
            if (users.length === 0) {
                //No user with that email exists
                var viewModel = new HomeViewModel();
                viewModel.session = req.session;
                viewModel.config = this._config;
                viewModel.error = "Invalid email or password";

                return res.render("login", viewModel);
            }

            var user = users[0];
            bcrypt.compare(req.body.password, user.password, function(err, result) {
                if (err) {
                    console.error(err);
                    return res.send('Something went wrong');
                }

                if (result === true) {
                    if (this._config.require_user_validation && !user.validated) {
                        //Tell the user they need to validate their account
                        return res.redirect('/register/validate');
                    }

                    req.session.userId = user._id;
                    res.redirect('/');
                } else {
                    console.log('[' + Date.now().toString() + ']' + 'User ' + user.email + ' failed to login');
                    
                    var viewModel = new HomeViewModel();
                    viewModel.session = req.session;
                    viewModel.config = this._config;
                    viewModel.error = "Invalid email or password";

                    res.render("login", viewModel);
                }
            }.bind(this));
        }.bind(this));
    }

    logOut(req, res, next) {
        var viewModel = new HomeViewModel();

        viewModel.config = this._config;
        req.session.userId = null;
        viewModel.session = req.session;

        res.redirect("/login");
    }
}

module.exports = AuthenticationController;
