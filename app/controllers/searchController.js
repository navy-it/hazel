"use strict";

const SearchViewModel = require("../models/searchViewModel");

class SearchController {
    constructor(server, config, authMethod, searchProvider) {
        this._server = server;
        this._config = config;
        this._auth = authMethod;
        this._searchProvider = searchProvider;

        this._bindRoutes();
    }

    _bindRoutes() {
        // /search
        this._server.get("/search", this._auth, this.index.bind(this));
    }

    /**
     * Render the search page
     */
    index(req, res, next) {
        var viewModel = new SearchViewModel();

        if (req.query.s) {
            var term = decodeURIComponent(req.query.s);
            viewModel.searchTerm = term;
            this._searchProvider.search(term, function(err, documents) {
                if (err) {
                    return console.error(err);
                }


                viewModel.searchResults = documents;

                console.log(viewModel.searchResults);

                viewModel.config = this._config;
                viewModel.session = req.session;
                return res.render("search", viewModel);
            }.bind(this));
        } else {

            viewModel.config = this._config;
            viewModel.session = req.session;
            res.render("search", viewModel);
        }
    }
}

module.exports = SearchController;
