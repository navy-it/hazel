"use strict";

const marked = require("marked");
const Document = require("../models/document");
const DetailViewModel = require("../models/detailViewModel");
const EditViewModel = require("../models/editViewModel");
const _ = require("lodash");
const moment = require("moment");

function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

class DocumentController {
    constructor(server, config, authMethod, documentRepository, analyticsService, storageProvider, searchProvider, parserUtility) {
        this._server = server;
        this._config = config;
        this._auth = authMethod;
        this._documents = documentRepository;
        this._analyticsService = analyticsService;
        this._storageProvider = storageProvider;
        this._searchProvider = searchProvider;
        this._parserUtility = parserUtility;

        this._bindRoutes();
    }

    _bindRoutes() {
         // /[slug]/edit
        this._server.get("/:slug/edit", this._auth, this.edit.bind(this));
        // /[slug]/delete
        this._server.get("/:slug/delete", this._auth, this.delete.bind(this));
        // /[slug]/save
        this._server.post("/:slug/save", this._auth, this.save.bind(this));
        // /[slug]/revisions
        this._server.get("/:slug/revisions", this._auth, this.revisions.bind(this));
        // /[slug]/revisions/:revisionId
        this._server.get("/:slug/revisions/:revisionId", this._auth, this.viewRevision.bind(this));
        // /[slug]/revisions/:revisionId/restore
        this._server.post("/:slug/revisions/:revisionId/restore", this._auth, this.restoreRevision.bind(this));
        // /new
        this._server.get("/new", this._auth, this.new.bind(this));
        // /[slug]
        this._server.get("/:slug", this._auth, this.detail.bind(this));
        // /upload
        this._server.post("/upload", this._auth, this.upload.bind(this));
    }

    /**
     * GET : Default Request Handler
     */
    detail(req, res, next) {
        if (!req.params.slug) { return next(); }

        let viewModel = new DetailViewModel();
        let slug = req.params.slug;

        this._documents.get(slug, (err, document) => {
            if (err) {
                console.error(err);
                return next();
            }
            if (!document || document.markdown <= 0) { next(); return; }

            this._analyticsService.updateViewCount(slug);
            document.html = marked(document.markdown);
            document.updateDate = moment(document.updateDate).fromNow();
            document.createDate = moment(document.createDate).fromNow();

            document.revisions = _.chain(document.revisions)
              .sortBy("date")
              .reverse()
              .value();

            for (var i = 0; i < document.revisions.length; i++) {
                var rev = document.revisions[i];

                document.revisions[i].date = moment(rev.date).fromNow();
                document.revisions[i].type = capitalizeFirstLetter(document.revisions[i].type);
            }

            viewModel.document = document;
            console.log(JSON.stringify(document, null, 4));
            viewModel.title = document.title;
            this._fetchRelatedDocuments(viewModel.title, 5, (err, documents) => {
                if (err) {
                    return console.error(err);
                }

                viewModel.relatedDocuments = documents;
                viewModel.recentDocuments = this._fetchRecentDocuments(5);
                viewModel.config = this._config;
                viewModel.session = req.session;
                viewModel.readonly = false;
                // render content
                res.render("document", viewModel);
            });
        });
    }

    revisions(req, res, next) {
        if (!req.params.slug) { return next(); }

        let viewModel = new DetailViewModel();
        let slug = req.params.slug;

        this._documents.get(slug, (err, document) => {
            // check if no content
            if (!document || document.markdown <= 0) { next(); return; }

            this._analyticsService.updateViewCount(slug);
            document.html = marked(document.markdown);
            document.updateDate = moment(document.updateDate).fromNow();
            document.createDate = moment(document.createDate).fromNow();

            document.revisions = _.chain(document.revisions)
              .sortBy("date")
              .reverse()
              .value();

            for (var i = 0; i < document.revisions.length; i++) {
                var rev = document.revisions[i];

                document.revisions[i].date = moment(rev.date).fromNow();
                document.revisions[i].type = capitalizeFirstLetter(document.revisions[i].type);
            }

            viewModel.document = document;
            viewModel.title = document.title;
            viewModel.config = this._config;
            viewModel.session = req.session;
            // render content
            res.render("documentRevisions", viewModel);
        });
    }

    viewRevision(req, res, next) {
        if (!req.params.slug || !req.params.revisionId) { return next(); }

        let viewModel = new DetailViewModel();
        let slug = req.params.slug;

        this._documents.getRevision(slug, req.params.revisionId, (err, document) => {
            if (err) {
                console.error(err);
                return res.send(err);
            }

            if (!document || document.markdown <= 0) { next(); return; }

            document.html = marked(document.markdown);
            document.updateDate = moment(document.updateDate).fromNow();
            document.createDate = moment(document.createDate).fromNow();

            document.revisions = _.chain(document.revisions)
              .sortBy("date")
              .reverse()
              .value();

            for (var i = 0; i < document.revisions.length; i++) {
                var rev = document.revisions[i];

                document.revisions[i].date = moment(rev.date).fromNow();
            }

            viewModel.document = document;
            viewModel.title = document.title;
            viewModel.config = this._config;
            viewModel.session = req.session;
            viewModel.readonly = true;
            viewModel.revisionId = req.params.revisionId;
            // render content
            res.render("document", viewModel);
        });
    }

    restoreRevision(req, res, next) {
        this._documents.restoreRevision(req.params.slug, req.params.revisionId, req.session.userId, (err) => {
            if (err) {
                console.error(err);
                return res.send("There was an error restoring the file");
            }

            return res.redirect("/"+req.params.slug);
        });
    }

    /**
     * GET : Handle edit request
     */
    edit(req, res, next) {
        if (!req.params.slug) { next(); return; }

        let slug = req.params.slug;
        console.log("editing: " + slug);

        let document = this._documents.get(slug, (err, document) => {
            if (err) return console.error(err);

            if (!document) {
                document = new Document();
                document.slug = slug;
                document.title = this._storageProvider.slugToTitle(slug);
            }

            let viewModel = new EditViewModel();
            viewModel.title = document.title;
            viewModel.document = document;
            viewModel.config = this._config;

            res.render("edit", viewModel);
        });
    }

    /**
     * POST : Handle edit request
     */
    save(req, res, next) {
        if (!req.params.slug) { next(); return; }

        let slug = req.params.slug;
        let originalSlug = req.body.originalSlug;

        // create document from form
        let document = new Document();
        document.title = req.body.title;
        document.markdown = req.body.content;
        document.tags = req.body.tags.split(",");
        document.slug = slug;
        document.updateDate = Date.now();
        document.revisionInfo = {
            user: req.session.userId
        };

        // check for links to set
        var missingLinks = this._parserUtility.fetchMissingLinksFromMarkdown(document.markdown);
        if (missingLinks && missingLinks.length > 0) {
            var pairs = _.chunk(missingLinks, 2);

            _.forEach(pairs, (pair) => {
                let slug = this._storageProvider.titleToSlug(pair[1]);
                document.markdown = document.markdown.replace(pair[0], pair[0].replace("()", "(/" + slug + ")"));
            });
        }

        // save document
        console.log("save: " + document.title);
        this._documents.update(document, (err, savedDoc) => {
            if (err) {
                console.error(err);
                return res.send(err);
            }

            this._searchProvider.indexUpdate(document);

            // remove old document if one existed
            if (originalSlug && originalSlug.length > 0 && originalSlug !== slug) {
                console.log("removing old document: " + originalSlug);
                this._documents.delete(originalSlug, (err) => {
                    if (err) return console.error(err);
                    this._searchProvider.indexRemove({ slug: originalSlug});
                });
            }

            return res.redirect(this._config.base + slug);
        });
    }

    /**
     * GET : Handle delete request
     */
    delete(req, res, next) {
        if (!req.params.slug) { next(); return; }

        let slug = req.params.slug;
        console.log("deleting: " + slug);

        let document = this._documents.get(slug, (err, document) => {
            if (err) {
                return console.error(err);
            }

            if (document) {
                this._documents.delete(slug, (err) => {
                    if (err) {
                        return console.error(err);
                    }

                    this._searchProvider.indexRemove({slug: slug});
                    res.redirect(this._config.base);
                });
            } else { next(); return; }
        });
    }

     /**
     * GET : Handle new request
     */
    new(req, res, next) {
        console.log("creating new document");

        let document = new Document();
        document.slug = "";
        document.title = "New document";

        let viewModel = new EditViewModel();
        viewModel.document = document;
        viewModel.title = document.title;
        viewModel.config = this._config;

        res.render("edit", viewModel);
    }

    /**
     * POST : Image uploading handler
     */
    upload(req, res, next) {
        this._storageProvider.storeFile(req, res, function(err) {
            if(err) {
                return res.status(422).send(err);
            }
            return res.status( 200 ).send( req.file.filename );
        });
    }

    /**
     * Fetch the most recent documents
     */
    _fetchRecentDocuments(count) {
        /*let documents = this._documents.all();

        return _.chain(documents)
            .reject({"updateDate": null})
            .sortBy("updateDate")
            .reverse()
            .take(count)
            .value();*/
            return [];
    }

    /**
     * Fetch the related documents
     */
    _fetchRelatedDocuments(title, count, callback) {
        console.log('fetching related documents');
        this._searchProvider.internalSearch(title, (err, documents) => {
            if (err) {
                return callback(err);
            }

            let docs = _.chain(documents)
                        .reject({"title": title})
                        .take(count)
                        .value();

            callback(null, docs);
        });
    }
}

module.exports = DocumentController;
