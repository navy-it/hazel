"use strict";

const TagViewModel = require("../models/tagViewModel");
const _ = require("lodash");
const marked = require("marked");
const moment = require("moment");

class TagController {
    constructor(server, config, authMethod, documentRepository, searchProvider, analyticsService) {
        this._documents = documentRepository;
        this._auth = authMethod;
        this._server = server;
        this._config = config;
        this._searchProvider = searchProvider;
        this._analyticsService = analyticsService;

        this._bindRoutes();
    }

    _bindRoutes() {
        // /
        this._server.get("/~:tag/", this._auth, this.index.bind(this));
    }

    /**
     * Render the tagpage
     */
    index(req, res, next) {
        if (!req.params.tag) { next(); return; }

        console.log(moment(new Date()).fromNow());

        var viewModel = new TagViewModel();

        viewModel.popularSearches = this._searchProvider.getPopularSearchTerms(5);
        viewModel.taggedDocuments = this._fetchTaggedDocuments(100, req.params.tag, (err, documents) => {
            if (err) {
                return console.error(err);
            }

            viewModel.taggedDocuments = documents;

            viewModel.section = _.chain(this._config.site_sections).filter(function(section) { return section.tag == req.params.tag; }).first().value();

            viewModel.config = this._config;
            viewModel.session = req.session;
            viewModel.moment = moment;

            res.render("tag", viewModel);
        });
    }


    /**
     * Fetch the tagged documents
     */
    _fetchTaggedDocuments(count, tag, callback) {
        this._documents.queryFindDocuments({'tags.0': {$exists: true}, tags: tag}, (err, documents) => {
            if (err) {
                return callback(err);
            }

            documents = _.chain(documents)
                        .sortBy("updateDate")
                        .reverse()
                        .take(count)
                        .value();

            documents.map((document) => {

            });

            callback(null, documents);
        });
    }

}

module.exports = TagController;
