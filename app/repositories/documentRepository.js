"use strict";

const _ = require("lodash");

/**
 * Document Repository used to manage and read from
 * the collection of documents. Creates a shallow clone
 * of all documents before sending for consumption in order
 * to enforce using the repo for CRUD operations.
 */
class DocumentRepository {

    constructor(documentStorageProvider) {
        this._storageProvider = documentStorageProvider;
    }

    /**
     * Return all documents in the repository
     */
    all(callback) {
        this._storageProvider.getAllDocuments((err, docs) => {
            if (err) {
                return callback(err);
            }

            return callback(null, docs);
        });
    }

    /**
     * Return a single document that matches the
     * provided slug
     */
    get(slug, callback) {
        this._storageProvider.readDocument(slug, function (err, document) {
            if (err) {
                return callback(err);
            }

            console.log("DocumentRepository", document);
            return callback(null, document);
        });
    }

    /**
     * Return a single document that matches the
     * provided revisionId
     */
    getRevision(slug, revisionId, callback) {
        this._storageProvider.queryFindRevisions(slug, revisionId, function (err, document) {
            if (err) {
                return callback(err);
            }

            return callback(null, document);
        });
    }

    restoreRevision(slug, revisionId, userId, callback) {
        this._storageProvider.restoreRevision(slug, revisionId, userId, (err) => {
            return callback(err);
        });
    }

    /**
     * Add a document to the repository
     */
    add(document, callback) {
        var existing = _.find(this._documents, { "slug": document.slug });
        this._storageProvider.hasDocument(document.slug, (err, exists) => {
            if (err) {
                return callback(err);
            }

            if (!exists) {
                document.createDate = Date.now();
                document.updateDate = document.createDate;
                document.tags = document.tags || [];
                this._storageProvider.storeDocument(document, (err, savedDoc) => {
                    if (err) {
                        return callback(err);
                    }

                    return callback(null, savedDoc);
                });
            }
        });
    }

    /**
     * Update an existing document in the repository
     */
    update(document, callback) {
        document.updateDate = Date.now();
        document.tags = document.tags || [];


        console.log("Attempting to store");
        this._storageProvider.storeDocument(document, callback);
    }

    /**
     * Query for a document using mongo query
     */
     queryFindDocuments(query, callback) {
         this._storageProvider.queryFindDocuments(query, callback);
     }

    /**
     * Delete a document from the repository
     */
    delete(slug, callback) {
        this._storageProvider.deleteDocument({slug: slug}, (err) => {
            return callback(err);
        });
    }
}

module.exports = DocumentRepository;
