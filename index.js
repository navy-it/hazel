/*!
 * hazel
 * Copyright(c) 2016 Wade Kallhoff
 * MIT Licensed
 */

"use strict";

module.exports = {
    app: require("./app/hazel"),
    storageProvider: require("storage-provider")
};
